<?php 
// El autoload es una ventaja que Composer nos brinda
// y nos ahorra un montón de llamadas a require()
require __DIR__ . '/vendor/autoload.php';

// Con la instrucción "use" le indicamos a PHP dónde puede encontrar la clase
use HolaMundo\Tiempo;

// Una vez cargada la clase con el mecanismo de "autoload" e importada con "use"
// podemos hacer uso de la clase.
echo "La fecha actual es: " . Tiempo::fecha();
?>

