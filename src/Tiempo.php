<?php
namespace HolaMundo;

class Tiempo {

    /**
     * Devuelve la fecha actual
     *
     * @return void
     */
    public static function fecha() {
        return date("Y-m-d");
    }

}
